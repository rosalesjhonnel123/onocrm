<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Asciisd\Zoho\Facades\ZohoManager;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // we can now deals with leads module
        $leads = ZohoManager::useModule('Leads');
        $lead = $leads->getRecord('4845958000000308059');
        print_r($leads->getRecords()); 
    }        
}
